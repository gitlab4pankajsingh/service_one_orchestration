package com.example.serviceOne.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import com.example.serviceOne.dao.Note;

@RestController
@RequestMapping("/serviceone")
public class ApplicationOne {
    @Autowired
    RestTemplate restTemplate;

    @Autowired
    private Environment env;

    final String host = System.getenv("SECOND_SERVICE_HOST");
    final String port = System.getenv("SECOND_SERVICE_PORT");

    @RequestMapping("/")
    public String home() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        return restTemplate.exchange(
                "http://" + host + ":" + port + "/servicetwo/products",
                HttpMethod.GET, entity, String.class).getBody();
    }

    @RequestMapping("/note/{desc}")
    public String home(@PathVariable("desc") final String desc) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        return restTemplate.exchange(
                "http://" + host + ":" + port + "/servicetwo/notes/" + desc,
                HttpMethod.GET, entity, String.class).getBody();
    }

    @RequestMapping("/note/all")
    public List<Note> getAllNotes() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        return restTemplate
                .exchange("http://" + host + ":" + port + "/servicetwo/notes",
                        HttpMethod.GET, entity, List.class)
                .getBody();
    }
    // 553 521 815
    // redhat1234
}