#From tomcat:8.0.51-jre8-alpine

#RUN rm -rf /usr/local/tomcat/webapps/*

#COPY ./target/serviceOne-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/ROOT.war

#CMD ["catalina.sh","run"]
#==============================================
#FROM openjdk:8-jre-alpine
#ENV APP_FILE serviceOne-0.0.1-SNAPSHOT.jar
#ENV APP_HOME /usr/apps
#EXPOSE 9090
#COPY target/$APP_FILE $APP_HOME/
#WORKDIR $APP_HOME
#ENTRYPOINT ["sh", "-c"]
#CMD ["exec java -jar $APP_FILE"]
#==============================================

FROM openjdk:8-jre-alpine
ADD target/serviceOne-0.0.1-SNAPSHOT.jar serviceOne-0.0.1-SNAPSHOT.jar
EXPOSE 9090
ENTRYPOINT ["java","-agentlib:jdwp=transport=dt_socket,address=5005,server=y,suspend=n","-Djava.security.egd=file:/dev/./urandom","-Xms2g","-Xmx8g","-jar","serviceOne-0.0.1-SNAPSHOT.jar"] 